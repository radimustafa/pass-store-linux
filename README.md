# Pass_Store

[Android App](https://play.google.com/store/apps/details?id=com.passwordsstore.passStore)

[App website](https://www.passwords-store.com/)

## Installation

```shell
git clone https://gitlab.com/radimustafa/pass-store-linux.git
cd pass-store-linux
sudo ./install.sh

# launch the app
pass_store
```

## How to use

The app on first launch will generate an encryption key which will be used to encrypt and decrypt the passwords.

It won't be possible to decrypt the passwords when losing the encryption key, therefore, it must be **manually** downloaded and saved in a safe place.

PS: You can generate different keys using Pass Store or uploading existing ones.

To generate a new key follow the below steps:

1. Generate an encryption key. _[1]_
- In Home screen tab on **I do not have key**
- From the drawer tab on **Generate New Key**

2. Save the key.
- Tab on **Save QR Code** to save it as PNG image. _[2]_
    - Enter the image name and press **Save**
- Tab on **Download PEM File** or **Download TXT File** to save the key as a file with desired extension. _[3]_
    - Enter the file name and press **Save**
    - Give the app permission to access your Downloads folder, to save the file there.

3. Upload the key. _[4]_
- In Home screen tab on **I have key**
- From the drawer tab on **Upload Key**

After uploading the key successfully,
you will be redirected to the **Passwords Store** screen where you can start adding encrypted passwords.

> **_IMPORTANT:_** Since the passwords are encrypted before storing them, it won't be possible to restore them if the key is lost.

---

_[1]_: Our service does not store passwords in a plain text anywhere nor in any way.
Therefore, we let the user generate an encryption key to be used for encrypting and decrypting the passwords.

_[2]_: The QR code can be printed or stored on another device so that it can be scanned to upload the key.

_[3]_: The key must be saved in a safe place and make sure that it will not be lost.

_[4]_: This step is necessary to ensure that the user knows where the key is stored.
