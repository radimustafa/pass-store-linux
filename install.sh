#!/bin/bash

echo "Installing pass_store..."

cp -TR src /usr/local/lib/pass_store/

echo "pass_store lib was installed."

ln -sf /usr/local/lib/pass_store/pass_store /usr/local/bin/pass_store

echo "pass_store binary was linked successfully"

echo "pass_store was installed successfully"
